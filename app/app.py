#!/usr/bin/python3

# creation serveur web
from flask import Flask
app = Flask(__name__)

@app.route("/") #exposition endpoint en GET
def hello():
  return "Hi oceane!"
#retoune Hi pour chaque requete recue

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=8000) #ecoute sur port 8000


